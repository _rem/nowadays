# -*- coding: utf-8 -*-
# -*- frozen_string_literal: true -*-

require "_config/minitest"
require_relative "./../../../dev/tasks/prep_release"

class TestPrepRelease < Minitest::Test
  # Don't parallelize_me!

  def test_can_build_gem_without_warnings
    skip "Integration: Building gems takes too long" unless ENV["NOWADAYS_ALL_TESTS"]
    _, err = capture_subprocess_io { PrepRelease.package }
    refute_match %r/warning/i, err
  end
end
