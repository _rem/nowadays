# -*- coding: utf-8 -*-
# -*- frozen_string_literal: true -*-

require "_config/minitest"
require "rbconfig"

class TestNowadaysBin < Minitest::Test
  # Don't parallelize_me!

  def test_binstubs_return_usage_by_default
    skip "slow CLI Integration" unless ENV["NOWADAYS_ALL_TESTS"]

    host = RbConfig::CONFIG["host_vendor"]
    nowadays = "nowadays"
    nowadays += %r"alpine".match?(host) ? "-#{host}" : ""

    out, _ = capture_subprocess_io { system nowadays }
    assert_match %r"usage:"i, out
  end

  def test_binstubs_permissions
    nowadays = "bin/nowadays"

    assert File.executable_real? nowadays
    assert File.executable_real? "#{nowadays}-alpine"
  end
end
