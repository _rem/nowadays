# -*- coding: utf-8 -*-
# -*- frozen_string_literal: true -*-
require_relative "./../../_config/minitest"
require "nowadays/manifest"

class TestNowadaysManifest < Minitest::Test
  parallelize_me!

  def test_has_gem_name
    assert_equal "nowadays", Nowadays::Manifest.gem_name
  end

  def test_has_official_name
    assert_equal "Nowadays", Nowadays::Manifest.official_name
  end

  def test_has_authors
    expected_authors = ["René Maya"]
    authors = Nowadays::Manifest.authors

    assert_equal expected_authors, authors
    assert_frozen authors
  end

  def test_has_contact_emails
    expected_emails = %w[rem@disroot.org]
    emails = Nowadays::Manifest.contact_emails
    assert_equal expected_emails, emails
    assert_frozen emails
  end

  def test_has_repo
    assert_equal "https://notabug.org/rem/nowadays.git", Nowadays::Manifest.repo
  end

  def test_has_license
    assert_equal "ISC", Nowadays::Manifest.license
  end

  def test_has_files
    # test/dev/**/*rb files are excluded because they are development helpers
    expected_files =
      %w[ChangeLog.md License.md Manifest.md ReadMe.md _expand_lib_path.rb] +
      Dir["bin/*"] + Dir["{lib,test}/**/*.rb"] - Dir["test/dev/**/*rb"]

    # assert_frozen missing. Apparently Gem::Specification mutates it.
    # must be unordered because CI
    assert_equal_unordered expected_files, Nowadays::Manifest.files
  end

  def test_codebase_returns_required_paths_basenames
    lib = "lib"
    codebase = Nowadays::Manifest.codebase

    assert Nowadays::Manifest.files.any? { |f| %r[\A#{lib}/(.*)].match? f }
    assert_equal [lib], codebase
    assert_frozen codebase
  end

  def test__test_suite_returns_test_dir_basename
    suite = "test"
    assert_equal suite, Nowadays::Manifest.test_suite
    assert Nowadays::Manifest.files.any? { |f| %r[\A#{suite}/(.*)].match? f }
  end

  def test__tests_are_included_in_the_manifest
    tests = Nowadays::Manifest.tests
    files = Nowadays::Manifest.files

    tests.each { |t| assert_includes files, t }
    assert_frozen tests
  end


  def test_has_bindir
    assert_equal "bin", Nowadays::Manifest.bindir
  end

  def test_binstubs_are_among_files_in_the_manifest
    binstubs = Nowadays::Manifest.binstubs
    files = Nowadays::Manifest.files
    bindir = Nowadays::Manifest.bindir

    binstubs.each { |b| assert_includes files, "#{bindir}/#{b}" }
    assert_frozen binstubs
  end
end
