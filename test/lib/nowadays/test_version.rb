# -*- coding: utf-8 -*-
# -*- frozen_string_literal: true -*-

require "_config/minitest"
require "nowadays/version"

class TestNowadaysVersion < Minitest::Test
  parallelize_me!

  def test_nowadays_has_a_version
    assert_match %r{\d+.\d+.\d+}, Nowadays::Version
  end
end
