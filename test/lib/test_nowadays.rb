# -*- coding: utf-8 -*-
# -*- frozen_string_literal: true -*-
require_relative "./../_config/minitest"
require "nowadays"

class TestNowadays < Minitest::Test
  parallelize_me!

  def test_nowadays
    assert Nowadays
  end
end
