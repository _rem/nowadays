# Nowadays

## Details

:TODO:

## System Requirements

- *nix OS.
- Ruby 2.5+

## Installation

Add to a project's Gemfile:

```ruby
gem "nowadays"
```

And then run

```bash
$ bundle
```

Or to install the command line interface locally:

```bash
$ gem install nowadays
```

## Quick Start

:TODO: Add usage examples here.

## License

Copyright (c) 2018, René Maya. Licensed ISC. Read attached `License.md` file
for details.
