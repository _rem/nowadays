# -*- coding: utf-8 -*-
# -*- frozen_string_literal: true -*-

require_relative "./_expand_lib_path"
require "nowadays/version"
require "nowadays/doc"
require "nowadays/manifest"

Gem::Specification.new do |g|
  g.name          = Nowadays::Manifest.gem_name
  g.version       = Nowadays::Version.dup
  g.authors       = Nowadays::Manifest.authors
  g.email         = Nowadays::Manifest.contact_emails
  g.summary       = Nowadays::Doc.summary
  g.description   = Nowadays::Doc.description
  g.homepage      = Nowadays::Manifest.repo
  g.license       = Nowadays::Manifest.license
  g.files         = Nowadays::Manifest.files
  g.require_paths = Nowadays::Manifest.codebase
  g.test_files    = Nowadays::Manifest.tests
  
  g.bindir        = Nowadays::Manifest.bindir
  g.executables.append(*Nowadays::Manifest.binstubs)
  
  g.extra_rdoc_files = Nowadays::Doc.appendices
  g.rdoc_options = Nowadays::Doc.build_options

  g.required_ruby_version = ">= 2.5"

  g.add_development_dependency "bundler", "~> 1.16", ">= 1.16.1"
  g.add_development_dependency "rake", "~> 12.3", ">= 12.3.0"
  g.add_development_dependency "hanna-nouveau", "~> 1.0", ">= 1.0.3"
  g.add_development_dependency "minitest", "~> 5.11", ">= 5.11.3"
  g.add_development_dependency "minitest-unordered", "~> 1.0", ">= 1.0.2"
  g.add_development_dependency "minitest-proveit", "~> 1.0", ">= 1.0.0"
  g.add_development_dependency "minitest-autotest", "~> 1.0", ">= 1.0.3"
end
