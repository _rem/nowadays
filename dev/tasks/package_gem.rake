# -*- coding: utf-8 -*-
# -*- frozen_string_literal: true -*-

require_relative "./prep_release"

desc "Pack gem"
task :pack => [:test] do
  PrepRelease.package
end
