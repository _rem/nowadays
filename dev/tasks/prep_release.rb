# -*- coding: utf-8 -*-
# -*- frozen_string_literal: true -*-
require "nowadays/manifest"

module PrepRelease
  def self.package

    # Unless many tasks require cleaning stuff don't require "rake/clean"
    # The more tasks the more unreadable `rake -T` becomes.
    gem = Nowadays::Manifest.gem_name
    old_gem = "./#{gem}-*.gem"

    File.delete(old_gem) if File.exist? old_gem
    system "gem", "build", "#{gem}"
  end
end
