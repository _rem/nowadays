# -*- coding: utf-8 -*-
# -*- frozen_string_literal: true -*-

require "nowadays/manifest"

class Nowadays

  # Aimed at gemspec, and supported doc generators.
  module Doc
    # One line summary of what Nowadays is.
    def self.summary
      "Profile web page generator"
    end

    # Short description of what Nowadays does, and how does it do it.
    def self.description
      <<~EOH
      Nowadays generates a single HTML page meant to answer "What you've been up to lately?".
      EOH
    end

    # Array of options used for building the documentation.
    def self.build_options
      [
        "--line-numbers",
        "--title", "#{Manifest.official_name}: #{self.summary}",
        "--main", "ReadMe.md"
      ]
    end

    # Array of documented files. These should be listed in Manifest.md.
    def self.files
      Nowadays::Manifest.files.select { |f|
        Nowadays::Manifest.codebase.any? { |dir| f.start_with? dir }
      }
    end

    # Array of extra doc files. These should be listed in Manifest.md.
    def self.appendices
      Nowadays::Manifest.files.select { |f| f.end_with? ".md" }
    end
  end
end
