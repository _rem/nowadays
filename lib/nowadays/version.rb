# -*- coding: utf-8 -*-
# -*- frozen_string_literal: true -*-

class Nowadays

  # Major version. Updated for changes that may require users to modify their code.
  MajorVersion = 0

  # Minor version. Updated for new feature releases.
  MinorVersion = 0

  # Patch version. Updated only for bug fixes from the last feature release.
  PatchVersion = 0

  # Full version as a string.
  Version = "#{MajorVersion}.#{MinorVersion}.#{PatchVersion}"
end
